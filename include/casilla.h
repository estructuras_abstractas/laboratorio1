#pragma once

/**
 * @author Alexander Calderón Torres
 * @date 14 de Enero de 2020
 */

#include <string>

using namespace std;

/**
 * @class Casilla
 * @brief Define las características generales de una casilla del tablero.
 */
class Casilla{
	public:
		Casilla();
		~Casilla();

		/**
		 * @brief Regresa el nombre de la casilla.
		 * @return El nombre de la casilla.
		*/
		string getNombre();
		
		/**
		 * @brief Regresa la posición que le corresponde a la casilla en el tablero.
		 * @return La posición en el tablero de la casilla.
		*/
		int getPosicion();
		
		/**
		 * @brief Regresa el tipo de casilla de la que se trata (GO, propiedad, compañía, impuestos, carcel o no_imp).
		 * @return El tipo de casilla de la que se trata.
		*/
		int getTipo();
		
		/**
		 * @brief Regresa el propietario actual de la casilla.
		 * @return El propietario actual de la casilla.
		*/
		string getPropietario();
		
		/**
		 * @brief Regresa el costo de compra/impuestos de la casilla.
		 * @return El precio de compra/impuestos de la casilla.
		*/
		int getValor();

		/**
		 * @brief Indica quién será el propietario.
		 * @param Nombre del futuro propietario.
		*/
		void setPropietario( string propietario );
		
		/**
		 * @brief Modifica todas las características de la casilla y las almacena en atributos privados.
		 * @param nombre Nombre de la casilla.
		 * @param posicion Posición en el tablero de la casilla.
		 * @param tipo Tipo de casilla (GO, propiedad, compañía, impuestos, cárcel o no_imp).
		 * @param valor Costo de compra/impuestos de la casilla.
		 	*/
		void setCaracteristicas( string nombre, int posicion, int tipo, int valor );

	private:
		string nombre;///Nombre de la casilla.
		string propietario;///A quien le pertenece la casilla.
		int    posicion;///Número que le corresponde a la casilla.

		int    tipo;//El tipo de casilla.
		/*  0: Casilla GO
			1: Propiedad
			2: Compañia
			3: Cobro de impuestos
			4: Carcel 
			5: No implementado */

		int valor;///El costo de la casilla al comprarla.
};