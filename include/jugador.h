#pragma once

/**
 * @author Jorge Munoz Taylor, Roberto Acevedo
 * @date 14/01/2020
 */

#include <string>

using namespace std;

/**
 * @class Jugador
 * @brief Se encarga de llevar a cabo toda la lógica del jugador de monopoly.
 */
class Jugador{
    private:

        string Nombre; ///< Nombre del jugador

        int    DineroJugador;///< Monto de dinero inicial del jugador

        int    Posicion;///< Posición en la que está el jugador.

        bool   Eliminado;///< Dice si el jugador está eliminado, true si lo está.
    
    public:
    
        Jugador (); 

        ~Jugador(); 

        /** @brief Define el nombre del jugador.
         *  @param nombre string con el nombre del jugador.
         */
        void setNombre(string nombre);
        

        /** @brief Guarda la cantidad de dinero en el atributo privado.
         *  @param dinerojugador Cantidad que se guardará.
         */
        void setDineroJugador(int dinerojugador);


        /** @brief Indica si el jugador fué eliminado.
         *  @param eliminado Booleano, true si el jugador perdió.
         */
        void setEliminado( bool eliminado );


        /** @brief Muestra el nombre del jugador.
         *  @return El nombre del jugador.
         */
        string getNombre();
        

        /** @brief Muestra cuanto dinero posee el jugador. 
         *  @return El dinero actual del jugador.
         */ 
        int getDineroJugador();


        /** @brief Genera un valor aleatorio entre 1 y 6 que representa un dado.
         *  @return El valor del dado.
         */
        int Tirar_Dados(); 


        /** @brief Modifica el dinero que posee el jugador, le suma o le resta.
         *  @param dinero_ganado_perdido Dinero que se sumará o restará.
         */
        void DineroTotal( int dinero_ganado_perdido ); 


        /** @brief Tira los dos dados e indica cuantos espacios en total debe moverse el jugador.
         *  @return La cantidad de casillas que debe moverse.
         */
        int Mover();


        /** @brief Regresa la posición actual del jugador.
         *  @return La posición actual.
         */
        int getPosicion();


        /** @brief Indica si el jugador ha sido eliminado de la partida.
         *  @return True si el jugador fue eliminado, false en caso contrario. 
         */
        bool getEliminado();


        /** @brief Cambia la posición actual del jugador en el tablero.
         *  @param posicion Nueva posición del jugador.
         */
        void setPosicion( int posicion );          
};