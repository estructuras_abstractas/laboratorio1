#pragma once

/**
 * @author Jorge Munoz Taylor
 * @date 14/01/2020
 */

#include "../include/casilla.h"
#include "../include/jugador.h"

/**
 * @class CONTROL
 * @brief Se encarga de llevar a cabo toda la lógica del Monopoly.
 */
class CONTROL
{
    public:
        
        /**
         * @brief Lleva toda la lógica del juego.
         */
        CONTROL();


        /**
         * @brief Destructor.
         */
        ~CONTROL();


        /**
         * @brief Define la cantidad de jugadores, sus nombres, su dinero y posición.
         * @param PLAYER
         * @return El número de jugadores de la partida.
         */
        int definir_jugadores( Jugador* &PLAYER );


        /**
         * @brief Muestra en pantalla las estadísticas del juego: cuanto dinero poseen los jugadores, propiedades y su posición.
         * @param CASILLAS Array que contiene todas las casillas del tablero.
         * @param PLAYER Array que contiene todos los jugadores de la partida.
         * @param Num_Jugadores Cantidad de jugadores de la partida.
         */
        void imprimir_estadisticas( Casilla*, Jugador*, int );


        /**
         * @brief Pausa el juego hasta que se oprima enter.
         */
        void pausar_juego();
    
};