/**
 * @author Jorge Munoz Taylor
 * @date 15/01/2020
 */

#include <iostream>
#include <string>
#include <stdio_ext.h>
#include "../include/control.h"

#define CANTIDAD_DE_CASILLAS 40
#define PLATA_INICIAL_JUGADOR 2000

#define GO 0
#define PROPIEDAD 1
#define COMPANIA 2
#define COBRO_IMPUESTOS 3
#define CARCEL 4
#define NO_IMP 5

using namespace std;


int CONTROL::definir_jugadores( Jugador* &PLAYER )
{
    string Nombre; ///< Guarda el nombre del jugador.
    int Num_Jugadores; ///< Guarda cuantos jugadores estarán en la partida.

    cout << endl << "* Número de jugadores: ";
    cin >> Num_Jugadores;

    if ( !cin ) ///< Verifica que el usuario introdujo un número.
    {
        cout << endl << " Escriba un número porfa :/" << endl;
        exit(0);
    }

    if ( Num_Jugadores < 2 || Num_Jugadores > 8)
    {
        cout << endl << " Escriba un número entre 2 y 8" << endl;
        exit(0);
    }

    cin.clear();

    PLAYER = new Jugador[ Num_Jugadores];

    for ( int i=0; i<Num_Jugadores; i++ )
    {
        cout << endl << "--> Nombre del jugador " << i+1 << " : ";
        cin >> Nombre;
        PLAYER[i].setNombre ( Nombre );
        PLAYER[i].setDineroJugador ( PLATA_INICIAL_JUGADOR );
        PLAYER[i].setPosicion( 0 );
        PLAYER[i].setEliminado(false );
        cin.clear();
    }

    cout << endl;
    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
    cout << endl << endl;
    return Num_Jugadores;
}


CONTROL::CONTROL()
{
    bool continuar_partida = true;
    int Numero_Jugadores;
    int Quien_Juega; ///< Indica cual jugador es el que está activo.

    int Posicion_Actual;
    int Numero_de_movimientos;
    int Contador_de_rondas = 1;
    int contador_de_victoria;
    string Comprar;
    bool continuar = true;

    Casilla* CASILLAS = new Casilla[ CANTIDAD_DE_CASILLAS ];
    Jugador* PLAYER; 

    //string nombre, int posicion, int tipo, int valor, int peaje
    CASILLAS [0].setCaracteristicas ( "Go", 0, GO, 0 );
    CASILLAS [1].setCaracteristicas ( "Mediterranean Avenue", 1, PROPIEDAD, 60 );
    CASILLAS [2].setCaracteristicas ( "Community Chest", 2, NO_IMP, 0 );
    CASILLAS [3].setCaracteristicas ( "Baltic Avenue", 3, PROPIEDAD, 60 );
    CASILLAS [4].setCaracteristicas ( "Tax", 4, COBRO_IMPUESTOS, 200 );
    CASILLAS [5].setCaracteristicas ( "Train", 5, COMPANIA, 200 );
    CASILLAS [6].setCaracteristicas ( "Oriental Avenue", 6, PROPIEDAD, 100 );
    CASILLAS [7].setCaracteristicas ( "Chance", 7, NO_IMP, 0 );
    CASILLAS [8].setCaracteristicas ( "Vermont Avenue", 8, PROPIEDAD, 100  );
    CASILLAS [9].setCaracteristicas ( "Connecticut Avenue", 9, PROPIEDAD, 120 );
    CASILLAS [10].setCaracteristicas( "Jail", 10, CARCEL, 50 );
    CASILLAS [11].setCaracteristicas( "St. Charles", 11, PROPIEDAD, 140 );
    CASILLAS [12].setCaracteristicas( "Electric Company", 12, COMPANIA, 150 );
    CASILLAS [13].setCaracteristicas( "States Avenue", 13, PROPIEDAD, 140 );
    CASILLAS [14].setCaracteristicas( "Virginia Avenue", 14, PROPIEDAD, 160 );
    CASILLAS [15].setCaracteristicas( "Pennsylvania Train", 15, COMPANIA, 200 );
    CASILLAS [16].setCaracteristicas( "James Place", 16, PROPIEDAD, 180 );
    CASILLAS [17].setCaracteristicas( "Community Chest", 17, NO_IMP, 0 );
    CASILLAS [18].setCaracteristicas( "Tennesse Avenue", 18, PROPIEDAD, 180 );
    CASILLAS [19].setCaracteristicas( "New York Avenue", 19, PROPIEDAD, 200 );
    CASILLAS [20].setCaracteristicas( "Parking", 20, NO_IMP, 0 );
    CASILLAS [21].setCaracteristicas( "Kentucky Avenue", 21, PROPIEDAD, 220 );
    CASILLAS [22].setCaracteristicas( "Chance", 22, NO_IMP, 0 );
    CASILLAS [23].setCaracteristicas( "Indiana Avenue", 23, PROPIEDAD, 220 );
    CASILLAS [24].setCaracteristicas( "Ilinois Avenue", 24, PROPIEDAD, 240 );
    CASILLAS [25].setCaracteristicas( "BAO Train", 25, COMPANIA, 200 );
    CASILLAS [26].setCaracteristicas( "Atlantic Avenue", 26, PROPIEDAD, 260 );
    CASILLAS [27].setCaracteristicas( "Ventnor Avenue", 27, PROPIEDAD, 260 );
    CASILLAS [28].setCaracteristicas( "Water Company", 28, COMPANIA, 150 );
    CASILLAS [29].setCaracteristicas( "Marvin Garden", 29, PROPIEDAD, 280 );
    CASILLAS [30].setCaracteristicas( "Jail", 30, CARCEL, 50 );
    CASILLAS [31].setCaracteristicas( "Pacific Avenue", 31, PROPIEDAD, 300 );
    CASILLAS [32].setCaracteristicas( "North Carolina Avenue", 32, PROPIEDAD, 300 );
    CASILLAS [33].setCaracteristicas( "Community Chest", 33, NO_IMP, 0 );
    CASILLAS [34].setCaracteristicas( "Pennsylvania Avenue", 34, PROPIEDAD, 320 );
    CASILLAS [35].setCaracteristicas( "Short Train", 35, COMPANIA, 200 );
    CASILLAS [36].setCaracteristicas( "Chance", 36, NO_IMP, 0 );
    CASILLAS [37].setCaracteristicas( "Park Place", 37, PROPIEDAD, 350 );
    CASILLAS [38].setCaracteristicas( "Luxury Tax", 38, COBRO_IMPUESTOS, 75 );
    CASILLAS [39].setCaracteristicas( "Board Walk", 39, PROPIEDAD, 400 );

    cout << endl << endl << "~~~~~~~~~~ MONOPOLY ~~~~~~~~~~" << endl;

    Numero_Jugadores = this->definir_jugadores( PLAYER );
    
    Quien_Juega = 0;

    while ( continuar_partida == true ) ///< Bucle principal de la partida.
    {
        continuar = true;

        if( PLAYER[ Quien_Juega ].getEliminado() == false  )
        {
            cout << endl;
            cout << "~~~~~~~~~~~ Ronda " << Contador_de_rondas << " ~~~~~~~~~~";
            cout << endl << endl;


            imprimir_estadisticas( CASILLAS, PLAYER, Numero_Jugadores );


            cout << "* Turno de " << PLAYER[ Quien_Juega ].getNombre() << ":";
            cout << endl << endl;

            ///Posición actual del jugador.
            Posicion_Actual = PLAYER[ Quien_Juega ].getPosicion();

            ///Indica cuantas casillas debe moverse el jugador.
            Numero_de_movimientos = PLAYER[ Quien_Juega ].Mover();


            for( int i=0; i<Numero_de_movimientos; i++ )
            {
            
                if( Posicion_Actual == CANTIDAD_DE_CASILLAS-1 )
                {
                    Posicion_Actual = 0;
                    PLAYER[ Quien_Juega ].DineroTotal( 200 );
                    cout << endl << endl;
                    cout << " -> " << PLAYER[Quien_Juega ].getNombre() << " pasó por la casilla GO";
                    cout << endl; 
                    cout << " --> " << PLAYER[Quien_Juega ].getNombre() << " recibe 200 al pasar por GO";
                    cout << endl << endl; 
                }
                else Posicion_Actual++;

            }//Fin de for

            PLAYER[ Quien_Juega].setPosicion( Posicion_Actual );


            switch ( CASILLAS[ Posicion_Actual].getTipo() )
            {
                case GO:
                    break;

                case PROPIEDAD:
                    if( CASILLAS[ Posicion_Actual ].getPropietario() == "nadie" )
                    {
                        while ( continuar == true)
                        {
                            cout << endl << "-> " << PLAYER[Quien_Juega].getNombre();
                            cout << ", quiere comprar la propiedad " << CASILLAS[ Posicion_Actual ].getNombre(); 
                            cout << "? ( escriba si o no ) ";
                            cin >> Comprar;

                            if( cin )
                            {
                                if( Comprar.compare("si") == 0 )
                                {
                                    CASILLAS[ Posicion_Actual ].setPropietario( PLAYER[ Quien_Juega ].getNombre() );

                                    PLAYER[ Quien_Juega ].DineroTotal( -CASILLAS[ Posicion_Actual ].getValor() );

                                    cout << endl;
                                    cout << "--> La propiedad " << CASILLAS[ Posicion_Actual ].getNombre();
                                    cout << " ahora le pertenece a " << PLAYER[ Quien_Juega ].getNombre();
                                    cout << endl;

                                    continuar = false;
                                }//Fin de if

                                else if( Comprar.compare("no") == 0 )
                                {
                                    continuar = false;
                                }

                            }//Fin de if

                            else cin.clear();
                        
                        }//Fin de while
                
                        cin.clear();
                    
                    }//Fin de if

                    else
                    {
                        ///Determina si la casilla tiene dueño.
                        ///Si tiene dueño, le suma el costo de la propiedad al dueño y le resta ese costo al otro jugador.
                        for ( int i=0; i<Numero_Jugadores; i++)
                        {
                            if ( CASILLAS[ Posicion_Actual ].getPropietario() == PLAYER[i].getNombre() )
                            {
                                PLAYER[i].DineroTotal( CASILLAS[ Posicion_Actual ].getValor() );
                                PLAYER[ Quien_Juega ].DineroTotal( -CASILLAS[ Posicion_Actual ].getValor());

                                cout << endl <<"-> La propiedad le pertenece a " << PLAYER[i].getNombre() << endl;
                                cout << "--> " << PLAYER[Quien_Juega].getNombre();
                                cout << " le paga " << CASILLAS[Posicion_Actual ].getValor();
                                cout << " a " << PLAYER[i].getNombre();
                                cout << endl;

                                i = Numero_Jugadores;
                            }//Fin de if

                        }//Fin de for

                    }//Fin de else
                    
                    break;


                case COMPANIA:
                    if( CASILLAS[ Posicion_Actual ].getPropietario() == "nadie" )
                    {
                        while ( continuar == true)
                        {
                            cout << endl << "-> " << PLAYER[Quien_Juega].getNombre();
                            cout << ", quiere comprar la compañía " << CASILLAS[ Posicion_Actual ].getNombre(); 
                            cout << "? ( escriba si o no ) ";
                            cin >> Comprar;

                            if( cin )
                            {
                                if( Comprar.compare("si") == 0)
                                {
                                    CASILLAS[ Posicion_Actual ].setPropietario( PLAYER[ Quien_Juega ].getNombre() );

                                    PLAYER[ Quien_Juega ].DineroTotal( -CASILLAS[ Posicion_Actual ].getValor() );

                                    cout << endl;
                                    cout << "--> La compañía " << CASILLAS[ Posicion_Actual ].getNombre();
                                    cout << " ahora le pertenece a " << PLAYER[ Quien_Juega ].getNombre();
                                    cout << endl;

                                    continuar = false;
                                }//Fin de if

                                else if( Comprar.compare("no") == 0 )
                                {
                                    continuar = false;
                                }

                            }//Fin de if

                            else cin.clear();
                        
                        }//Fin de while
                
                        cin.clear();
                    
                    }//Fin de if

                    else
                    {
                        ///Determina si la casilla tiene dueño.
                        ///Si tiene dueño, le suma el costo de la propiedad al dueño y le resta ese costo al otro jugador.
                        for ( int i=0; i<Numero_Jugadores; i++)
                        {
                            if ( CASILLAS[ Posicion_Actual ].getPropietario() == PLAYER[i].getNombre() )
                            {
                                PLAYER[i].DineroTotal( CASILLAS[ Posicion_Actual ].getValor() );
                                PLAYER[ Quien_Juega ].DineroTotal( -CASILLAS[ Posicion_Actual ].getValor());

                                cout << endl << "-> La compañía le pertenece a " << PLAYER[i].getNombre() << endl;
                                cout << "--> " << PLAYER[Quien_Juega].getNombre();
                                cout << " le paga " << CASILLAS[Posicion_Actual ].getValor();
                                cout << " a " << PLAYER[i].getNombre();
                                cout << endl;

                                i = Numero_Jugadores;
                            }//Fin de if

                        }//Fin de for

                    }//Fin de else
                    break;


                case COBRO_IMPUESTOS:
                    cout << endl << "-> Pagas " << CASILLAS[ Posicion_Actual ].getValor();
                    cout << " en impuestos :(" << endl;

                    PLAYER[ Quien_Juega ].DineroTotal( -CASILLAS[ Posicion_Actual ].getValor() );
                    break;


                case CARCEL:
                    cout << endl << "-> Pagas " << CASILLAS[ Posicion_Actual ].getValor();
                    cout << " para salir de la reforma ;D" << endl;

                    PLAYER[ Quien_Juega ].DineroTotal( -CASILLAS[ Posicion_Actual ].getValor() );
                    break;


                case NO_IMP: ///No implementado, no ocurre nada en el juego.

                    cout << "-> Casilla " << CASILLAS[ Posicion_Actual ].getNombre() << " su función no está implementada.";
                    cout << endl;
                    break;

                default:
                    cout << endl << endl << "Error de tipo de casilla" << endl << endl;
                    exit(0);
                    break;
                
            }//Fin de switch

            Contador_de_rondas++;

        }//Fin de if

        if ( Quien_Juega != Numero_Jugadores-1 )
            Quien_Juega++;
        else 
            Quien_Juega = 0;


        ///< Verifica si un jugador tiene las condiciones para ser eliminado.
        for ( int i=0; i<Numero_Jugadores; i++)
        {
            if( PLAYER[i].getDineroJugador() == 0 && PLAYER[i].getEliminado() == false )
            {
                PLAYER[i].setEliminado( true );
                cout << endl << endl;
                cout << "++++++++++++++++++++++++++++++";
                cout << endl << " El jugador " << PLAYER[i].getNombre() << " ha perdido >:)" << endl;
                cout << "++++++++++++++++++++++++++++++" << endl;

                ///< Libera las propiedades que posea el jugador eliminado.
                for ( int j=0; j<CANTIDAD_DE_CASILLAS; j++)
                { 
                    if ( CASILLAS [j].getPropietario() == PLAYER[i].getNombre() )
                    {
                        CASILLAS[j].setPropietario("nadie"); 
                    }

                }//Fin de for
            
            }//Fin de if

        }//Fin de for

        contador_de_victoria = 0;

        for ( int i=0; i<Numero_Jugadores; i++)
        {
            if ( PLAYER[i].getEliminado() == false )
            {
                contador_de_victoria++;
            } 
        }


        if( contador_de_victoria == 1)
        {
            for ( int i=0; i<Numero_Jugadores; i++)
            {
                if( PLAYER[i].getEliminado()==false )
                {
                    cout << endl << endl;
                    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
                    cout << "~~~~~~~~~~~~~ FIN ~~~~~~~~~~~~" << endl << endl;
                    cout << "--> " << PLAYER[i].getNombre() << " ha ganado la partida! :D" << " <--";
                    cout << endl << endl;
                    i = Numero_Jugadores;
                    continuar_partida = false;
                }
            }//Fin de for
        }//Fin de if
        else
        {
            this->pausar_juego();
        }

    }//Fin de while
    
}


CONTROL::~CONTROL()
{
    cout << endl << ">>> ¡Finalizó la partida! <<<" << endl;
}


void CONTROL::imprimir_estadisticas( Casilla* CASILLAS, Jugador* PLAYER, int Num_Jugadores )
{
    cout << "++++++++++++++++++++++++++++++";

    for ( int i=0; i<Num_Jugadores; i++)
    {
        if( PLAYER[i].getEliminado() == false )
        {
            cout << endl;
            cout << PLAYER[i].getNombre() << ":" << endl;
            cout << "--------> Posición: casilla " << PLAYER[i].getPosicion();
            cout << " (" << CASILLAS[ PLAYER[i].getPosicion() ].getNombre() <<")" << endl;

            cout << "--------> Dinero: " << PLAYER[i].getDineroJugador() << " piedrolares" << endl;

            cout << "--------> Propiedades:" << endl;
            
            for( int j=0; j<CANTIDAD_DE_CASILLAS; j++ )
            {
                if( CASILLAS[j].getPropietario() == PLAYER[i].getNombre() )
                {
                    cout << endl << "----------------> " << CASILLAS[j].getNombre();
                }//Fin de if

            }//Fin de for

            cout << endl;
        }

    }//Fin de for

    cout << "++++++++++++++++++++++++++++++" << endl << endl;
}


void CONTROL::pausar_juego()
{    
    __fpurge(stdin); ///< Limpia el buffer de cin.
    
    cout << endl;
    cout << "** Presione ENTER para continuar con el siguiente turno **";
    cin.ignore(); ///< Espera hasta que el usuario oprima enter.
       
    cout << endl;
}