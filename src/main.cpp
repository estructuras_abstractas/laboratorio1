#include <iostream>
#include "../include/control.h"

#define OK 0 ///< Usado para indicar que todo salió bien.

using namespace std;


/**
 * @brief Función main, declara un objeto tipo CONTROL, este se encarga de ejecutar el juego y toda su lógica.
 * @return Regresa OK(0) si la ejecución terminó correctamente.
 */

int main()
{
    CONTROL partida; ///< Declara objeto de tipo CONTROL.

    return OK;
}