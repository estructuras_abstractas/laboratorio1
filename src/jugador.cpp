#include <iostream>
#include <random> ///< Biblioteca para generar números pseudoaleatorios.
#include "../include/jugador.h"

using namespace std;

Jugador::Jugador(){}

Jugador::~Jugador(){}


void Jugador::setNombre(string nombre){
    this->Nombre = nombre;
}


void Jugador::setEliminado( bool eliminado )
{
    this->Eliminado = eliminado;
}


void Jugador:: setDineroJugador(int dinerojugador){
    this->DineroJugador = dinerojugador;
}


string Jugador:: getNombre(){
    return this->Nombre;
}

    
int Jugador:: getDineroJugador(){
    return this->DineroJugador;
}


int Jugador:: Tirar_Dados(){

    /**
     * Genera y devuelve un número pseudoaleatorio.
     */
    random_device generator; 
    mt19937 gen(generator());
    uniform_int_distribution<> distribution(1,6);

    int dado = distribution(generator);

    cout << "--> Salió un " << dado << endl;
    
    return (dado);
}


void Jugador:: DineroTotal( int dinero_ganado_perdido )
{
    int dinero;

    dinero = this->DineroJugador + dinero_ganado_perdido;

    if ( dinero <= 0 ) 
    {
        cout << endl << "El jugador " << this->Nombre << " ya no tiene dinero :(" << endl;

        setDineroJugador( 0 );
    }
    else
    {
        setDineroJugador( dinero );
    }

}


int Jugador::Mover()
{
    int dado1;
    int dado2;

    cout << "-> Primer dado:  ";
    dado1 = this->Tirar_Dados();

    cout << "-> Segundo dado: ";
    dado2 = this->Tirar_Dados();

    cout << endl << "--> El jugador "<<this->Nombre << " se mueve "<< dado1 + dado2;
    cout << " espacios." << endl; 

    return dado1 + dado2;
}


int Jugador::getPosicion()
{
    return this->Posicion;
}


bool Jugador::getEliminado()
{
    return this->Eliminado;
}


void Jugador::setPosicion( int posicion )
{
    this->Posicion = posicion;
}