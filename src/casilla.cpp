#include "../include/casilla.h"

using namespace std;


Casilla::Casilla(){}
Casilla::~Casilla(){}



string Casilla::getNombre(){return this->nombre;}
int Casilla::getPosicion(){return this->posicion;}
int Casilla::getTipo(){return this->tipo;}
string Casilla::getPropietario(){return this->propietario;}
int Casilla::getValor(){return this->valor;}
void Casilla::setPropietario(string propietario){this->propietario=propietario;}
void Casilla::setCaracteristicas(string nombre, int posicion, int tipo, int valor){
	this->nombre=nombre;
	this->posicion = posicion;
	this->tipo=tipo;
	this->propietario= "nadie";
	this->valor=valor;	
}