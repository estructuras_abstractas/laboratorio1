ALL: comp doxygen

comp:
	mkdir -p bin build docs

	g++ -o ./build/jugador.o -c ./src/jugador.cpp
	g++ -o ./build/casilla.o -c ./src/casilla.cpp
	g++ -o ./build/control.o -c ./src/control.cpp
	g++ -o ./build/main.o -c ./src/main.cpp
	
	g++ -o ./bin/monopoly ./build/main.o ./build/jugador.o ./build/casilla.o ./build/control.o

doxygen:
	mkdir -p docs
	doxygen Doxyfile
	make -C ./docs/latex