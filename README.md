# Laboratorio 1: Monopoly en C++


## Integrantes
```
Jorge Munoz Taylor
Alexander Calderon Torres
Roberto Acevedo Mora
```

## Como compilar el proyecto
Una vez descargado el código, ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/laboratorio1
```

Por último ejecute el make:
```
>>make
```
Esto compilará el programa y el doxygen.

## Ejecutar el programa
Simplemente:
```
./bin/monopoly
```

## PARA Doxygen
Ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/laboratorio1
```

Para compilar el doxygen hay dos formas:
```
>>make #Compila el proyecto y el doxygen
```
```
>>make doxygen #Sólo compila el doxygen
```

Ambos generarán el archivo PDF con la documentación creada por doxygen. Para verlo:
```
Doble clic al archivo refman.pdf dentro de docs/latex
```

## Dependencias
Debe tener instalado CMAKE y MAKE en la máquina:
```
>>sudo apt-get install build-essential
>>sudo apt-get install cmake
```
También doxygen y latex:
```
>>sudo apt install doxygen
>>sudo apt install doxygen-gui
>>sudo apt-get install graphviz
>>sudo apt install texlive-latex-extra
>>sudo apt install texlive-lang-spanish
```

## Tablero usado
![](tablero.jpg)